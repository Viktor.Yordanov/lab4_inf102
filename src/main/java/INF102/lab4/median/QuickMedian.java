package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        int k = listCopy.size() / 2; // k-th element for median (0-based index)

        // Find the k-th smallest element
        T median = quickSelect(listCopy, 0, listCopy.size() - 1, k);

        return median;
        
    }


    private <T extends Comparable<T>> T quickSelect(List<T> list, int left, int right, int k) {
        if (left == right) {
            return list.get(left);
        }

        // Partition the list and get the pivot index
        int pivotIndex = partition(list, left, right);

        if (k == pivotIndex) {
            return list.get(k);
        } else if (k < pivotIndex) {
            return quickSelect(list, left, pivotIndex - 1, k);
        } else {
            return quickSelect(list, pivotIndex + 1, right, k);
        }
    }

    private <T extends Comparable<T>> int partition(List<T> list, int left, int right) {
        T pivot = list.get(right);
        int i = left - 1;

        for (int j = left; j < right; j++) {
            if (list.get(j).compareTo(pivot) <= 0) {
                i++;
                swap(list, i, j);
            }
        }

        swap(list, i + 1, right);
        return i + 1;
    }

    private <T extends Comparable<T>> void swap(List<T> list, int i, int j) {
        T temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }

}
